import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';


const Input = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField
    hintText={label}
    floatingLabelText={label}
    errorText={touched && error}
    autoComplete="off"
    {...input}
    {...custom}
  />
);


Input.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  meta: PropTypes.object,
};

export default Input;
