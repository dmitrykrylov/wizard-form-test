import React from 'react';
import { Field } from 'redux-form/immutable';
import Input from 'components/Input';
import RaisedButton from 'material-ui/RaisedButton';


export default ({ handleSubmit }) => (
  <form onSubmit={handleSubmit}>
    <Field
      name="email"
      component={Input}
      label="Email"
    />
    <br />
    <Field
      name="password"
      type="password"
      component={Input}
      label="Password"
    />
    <br />
    <RaisedButton
      type="submit"
      label="Submit"
      primary
    />
  </form>
);
