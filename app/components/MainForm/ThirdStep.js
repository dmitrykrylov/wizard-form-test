import React from 'react';
import { Field } from 'redux-form/immutable';
import Input from 'components/Input';
import RaisedButton from 'material-ui/RaisedButton';


export default ({ handleSubmit }) => (
  <form onSubmit={handleSubmit}>
    <Field
      name="ccnum"
      type="text"
      component={Input}
      label="CCNUM"
    />
    <br />
    <Field
      name="expired"
      type="text"
      component={Input}
      label="Expired"
    />
    <br />
    <RaisedButton
      type="submit"
      label="Submit"
      primary
    />
  </form>
);
