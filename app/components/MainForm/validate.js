export default (values) => {
  const errors = {};

  console.log(values)

  if (!values.get('email')) {
    errors.email = 'This field is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('email'))) {
    errors.email = 'Invalid email address';
  }
  if (!values.get('password')) {
    errors.password = 'This field is required';
  } else if (values.get('password').length < 6) {
    errors.password = 'Password must be at least 6 characters';
  }

  if (!values.get('ccnum')) {
    errors.ccnum = 'This field is required';
  }
  if (!values.get('expired')) {
    errors.expired = 'This field is required';
  }

  return errors;
};
