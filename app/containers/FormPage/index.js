/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Step, Stepper, StepLabel } from 'material-ui/Stepper';
import { FormFirstStep, FormSecondStep, FormThirdStep } from './Form';


class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleNext = this.handleNext.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { stepIndex: 0 };
  }

  handleNext() {
    this.setState({ stepIndex: this.state.stepIndex + 1 });
  }

  handleSubmit() {
    this.props.push('/results');
  }

  render() {
    const { stepIndex } = this.state;

    return (
      <div>
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Plans</StepLabel>
          </Step>
          <Step>
            <StepLabel>Account</StepLabel>
          </Step>
          <Step>
            <StepLabel>Billing / Cart</StepLabel>
          </Step>
        </Stepper>
        { stepIndex === 0 && <FormFirstStep onSubmit={this.handleNext} /> }
        { stepIndex === 1 && <FormSecondStep onSubmit={this.handleNext} /> }
        { stepIndex === 2 && <FormThirdStep onSubmit={this.handleSubmit} /> }
      </div>
    );
  }
}


HomePage.propTypes = {
  push: React.PropTypes.func,
};


export default connect(null, { push })(HomePage);
