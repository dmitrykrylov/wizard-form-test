import { reduxForm } from 'redux-form/immutable';
import { FirstStep, SecondStep, ThirdStep, validate } from 'components/MainForm';


export const FormFirstStep = reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(FirstStep);


export const FormSecondStep = reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(SecondStep);


export const FormThirdStep = reduxForm({
  form: 'wizard',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
})(ThirdStep);
