/*
 *
 * ResultPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';


export class ResultPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount(props) {
    if (!this.props.formValues) {
      this.props.push('/');
    }
  }

  render() {
    const { formValues } = this.props;
    return (
      <div>
        <h1>Thank you!</h1>
        { this.props.formValues && <div><strong>Your Email: </strong>{formValues.email}</div>}
      </div>
    );
  }
}

ResultPage.propTypes = {
  formValues: PropTypes.object,
  push: PropTypes.func,
};


const mapStateToProps = (state) => {
  const form = state.get('form').get('wizard');
  return {
    formValues: form ? form.get('values').toJS() : null,
  };
};

export default connect(mapStateToProps, { push })(ResultPage);
